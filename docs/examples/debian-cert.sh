#!/bin/mksh
# -*- mode: sh -*-
#-
# Copyright © 2018, 2019, 2021, 2024
#	mirabilos <mirabilos@evolvis.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#-
# mkdir -p /usr/local/libexec
# install -c -o 0 -g 0 -m 500 docs/examples/debian-cert.sh /usr/local/libexec/
# - then edit to restart affected services
# - and add to sudoers:
#  _acme	ALL = NOPASSWD: /usr/local/libexec/debian-cert.sh

set -e
set -o pipefail
umask 077
cd /
set +e

if (( USER_ID )); then
	print -ru2 E: need root
	exit 1
fi

IFS= read -r line
if [[ $line != '# from debian-hook'?(-dns)'.sh' ]]; then
	print -ru2 E: not called from dehydrated hook script
	exit 1
fi

# work around https://community.letsencrypt.org/t/help-thread-for-dst-root-ca-x3-expiration-september-2021/149190/328
function not_blacklisted_chain {
	[[ $1 != '-----BEGIN CERTIFICATE-----
MIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
DkRTVCBSb290IENBIFgzMB4XDTIxMDEyMDE5MTQwM1oXDTI0MDkzMDE4MTQwM1ow
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwggIiMA0GCSqGSIb3DQEB
AQUAA4ICDwAwggIKAoICAQCt6CRz9BQ385ueK1coHIe+3LffOJCMbjzmV6B493XC
ov71am72AE8o295ohmxEk7axY/0UEmu/H9LqMZshftEzPLpI9d1537O4/xLxIZpL
wYqGcWlKZmZsj348cL+tKSIG8+TA5oCu4kuPt5l+lAOf00eXfJlII1PoOK5PCm+D
LtFJV4yAdLbaL9A4jXsDcCEbdfIwPPqPrt3aY6vrFk/CjhFLfs8L6P+1dy70sntK
4EwSJQxwjQMpoOFTJOwT2e4ZvxCzSow/iaNhUd6shweU9GNx7C7ib1uYgeGJXDR5
bHbvO5BieebbpJovJsXQEOEO3tkQjhb7t/eo98flAgeYjzYIlefiN5YNNnWe+w5y
sR2bvAP5SQXYgd0FtCrWQemsAXaVCg/Y39W9Eh81LygXbNKYwagJZHduRze6zqxZ
Xmidf3LWicUGQSk+WT7dJvUkyRGnWqNMQB9GoZm1pzpRboY7nn1ypxIFeFntPlF4
FQsDj43QLwWyPntKHEtzBRL8xurgUBN8Q5N0s8p0544fAQjQMNRbcTa0B7rBMDBc
SLeCO5imfWCKoqMpgsy6vYMEG6KDA0Gh1gXxG8K28Kh8hjtGqEgqiNx2mna/H2ql
PRmP6zjzZN7IKw0KKP/32+IVQtQi0Cdd4Xn+GOdwiK1O5tmLOsbdJ1Fu/7xk9TND
TwIDAQABo4IBRjCCAUIwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYw
SwYIKwYBBQUHAQEEPzA9MDsGCCsGAQUFBzAChi9odHRwOi8vYXBwcy5pZGVudHJ1
c3QuY29tL3Jvb3RzL2RzdHJvb3RjYXgzLnA3YzAfBgNVHSMEGDAWgBTEp7Gkeyxx
+tvhS5B1/8QVYIWJEDBUBgNVHSAETTBLMAgGBmeBDAECATA/BgsrBgEEAYLfEwEB
ATAwMC4GCCsGAQUFBwIBFiJodHRwOi8vY3BzLnJvb3QteDEubGV0c2VuY3J5cHQu
b3JnMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly9jcmwuaWRlbnRydXN0LmNvbS9E
U1RST09UQ0FYM0NSTC5jcmwwHQYDVR0OBBYEFHm0WeZ7tuXkAXOACIjIGlj26Ztu
MA0GCSqGSIb3DQEBCwUAA4IBAQAKcwBslm7/DlLQrt2M51oGrS+o44+/yQoDFVDC
5WxCu2+b9LRPwkSICHXM6webFGJueN7sJ7o5XPWioW5WlHAQU7G75K/QosMrAdSW
9MUgNTP52GE24HGNtLi1qoJFlcDyqSMo59ahy2cI2qBDLKobkx/J3vWraV0T9VuG
WCLKTVXkcGdtwlfFRjlBz4pYg1htmf5X6DYO8A4jqv2Il9DjXA6USbW1FzXSLr9O
he8Y4IWS6wY7bCkjCWDcRQJMEhg76fsO3txE+FiYruq9RUWhiF1myv4Q6W+CyBFC
Dfvp7OOGAN6dEOM4+qR9sdjoSYKEBpsr6GtPAQw4dy753ec5
-----END CERTIFICATE-----' ]]
}

nl=$'\n'
key=
cer=
chn=
buf=
s=0

while IFS= read -r line; do
	buf+=$line$nl
	[[ $line = '-----END'* ]] || continue
	case $s {
	(0)
		if ! key=$(print -nr -- "$buf" | \
		    sudo -u nobody openssl rsa) 2>&1; then
			print -ru2 E: could not read private key
			exit 1
		fi
		key+=$nl
		s=1
		;;
	(*)
		if ! line=$(print -nr -- "$buf" | \
		    sudo -u nobody openssl x509) 2>&1; then
			print -ru2 E: could not read certificate $s
			exit 1
		fi
		if (( s == 1 )); then
			cer=$line$nl
		elif not_blacklisted_chain "$line"; then
			chn+=$line$nl
		fi
		s=2
		;;
	}
	buf=
done

case $s {
(0)
	print -ru2 -- E: private key missing
	exit 1
	;;
(1)
	print -ru2 -- E: certificate missing
	exit 1
	;;
(2)
	if [[ -z $chn ]]; then
		print -ru2 -- E: expected a chain of at least length 1
		exit 1
	fi
	;;
(*)
	print -ru2 -- E: cannot happen
	exit 255
	;;
}

set -A rename_src
set -A rename_dst
nrenames=0
rv=0

function dofile {
	local mode=$1 owner=$2 name=$3 content=$4 fn oc

	(( rv )) && return

	# we cannot update symlinks (no matter whether the target exists)
	if [[ -h $name ]]; then
		print -ru2 "E: not a regular file: $name"
		rv=2
		return
	fi
	# not symlink, if it exists, check for regular file, try to read it
	[[ ! -e $name ]] || if [[ ! -f $name ]]; then
		print -ru2 "E: not a regular file: $name"
		rv=2
		return
	elif ! oc=$(cat "$name" && echo .); then
		print -ru2 "E: cannot read old $name"
	elif [[ ${oc%.} = "$content" ]]; then
		print -r "I: no change, skipping $name"
		chown "$owner" "$name" || rv=2
		chmod "$mode" "$name" || rv=2
		return
	fi
	unset oc

	if ! fn=$(mktemp "$name.XXXXXXXXXX"); then
		print -ru2 "E: cannot create temporary file for $name"
		rv=2
		return
	fi
	rename_src[nrenames]=$fn
	rename_dst[nrenames++]=$name
	chown "$owner" "$fn" || rv=2
	chmod "$mode" "$fn" || rv=2
	(( rv )) && return
	if ! print -nr -- "$content" >"$fn"; then
		print -ru2 "E: cannot write to temporary file for $name"
		rv=2
		return
	fi
}

if [[ -d /etc/.git && -x /usr/bin/git ]]; then
	if [[ -z $GIT_AUTHOR_EMAIL || -z $GIT_COMMITTER_EMAIL ]]; then
		[[ $HOSTNAME = nil ]] && unset HOSTNAME
		[[ -n $HOSTNAME ]] || HOSTNAME=$(hostname -f) || HOSTNAME=
		[[ -n $HOSTNAME ]] || HOSTNAME=$(hostname) || HOSTNAME=
		[[ -n $HOSTNAME ]] || HOSTNAME=localhost
	fi
	[[ -n $GIT_AUTHOR_NAME ]] || GIT_AUTHOR_NAME=dehydrated-mir
	[[ -n $GIT_AUTHOR_EMAIL ]] || GIT_AUTHOR_EMAIL=root@$HOSTNAME
	[[ -n $GIT_COMMITTER_EMAIL ]] || GIT_COMMITTER_EMAIL=root@$HOSTNAME
	export GIT_AUTHOR_NAME GIT_AUTHOR_EMAIL GIT_COMMITTER_EMAIL
	cd /etc
	of=
	for f in ssl/private/deflt+ca.pem ssl/private/default.key \
	    ssl/deflt+ca.pem ssl/dhparams.pem ssl/default.ca ssl/default.cer; do
		[[ -e $f || -h $f ]] && of+=$f' '
	done
	[[ -z $of ]] || git commit -m 'saving uncommitted changes to SSL certs before dehydrated-mir' $of
	cd /
fi

dhp=
if [[ -s /etc/ssl/dhparams.pem ]] && \
    ! dhp=$(</etc/ssl/dhparams.pem)$nl; then
	print -ru2 "E: could not read existing DH parameters"
	exit 2
fi
[[ -n $dhp ]] || if ! dhp=$(openssl dhparam 2048)$nl; then
	print -ru2 "E: could not generate new DH parameters"
	exit 2
fi

dofile 0644 0:0 /etc/ssl/default.cer "$cer$dhp"
dofile 0644 0:0 /etc/ssl/default.ca "$chn"
dofile 0644 0:0 /etc/ssl/dhparams.pem "$dhp"
dofile 0644 0:0 /etc/ssl/deflt+ca.pem "$cer$chn$dhp"
dofile 0640 0:ssl-cert /etc/ssl/private/default.key "$key"
dofile 0640 0:ssl-cert /etc/ssl/private/deflt+ca.pem "$key$cer$chn$dhp"

if (( rv )); then
	rm -f "${rename_src[@]}"
	exit $rv
fi

sync
while (( nrenames-- )); do
	print -r "I: updating ${rename_dst[nrenames]}"
	if ! mv "${rename_src[nrenames]}" "${rename_dst[nrenames]}"; then
		print -ru2 "E: rename ${rename_src[nrenames]@Q}" \
		    "${rename_dst[nrenames]@Q} failed ⇒ system hosed"
		while (( nrenames-- )); do
			print -ru2 "W: not updating ${rename_dst[nrenames]}"
		done
		exit 3
	fi
done
sync

if [[ -d /etc/.git && -x /usr/bin/git ]]; then
	cd /etc
	git add \
	    ssl/private/deflt+ca.pem ssl/private/default.key \
	    ssl/deflt+ca.pem ssl/dhparams.pem ssl/default.ca ssl/default.cer
	git -c gc.auto=670 commit \
	    -m 'committing changes to SSL certs from dehydrated-mir ' \
	    ssl/private/deflt+ca.pem ssl/private/default.key \
	    ssl/deflt+ca.pem ssl/dhparams.pem ssl/default.ca ssl/default.cer
	cd /
fi

readonly p=/bin:/usr/bin:/sbin:/usr/sbin
rc=0
function svr {
	local rv iserr=$1; shift
	/usr/bin/env -i PATH=$p HOME=/ "$@" 2>&1
	rv=$?
	(( rv )) && if (( iserr )); then
		print -ru2 "E: errorlevel $rv trying to $*"
		rc=1
	else
		print -ru1 "W: errorlevel $rv trying to $*"
	fi
	#(( rv )) || print -ru1 "I: ok trying to $*"
}

function tkill {
	local x=$*

	[[ -n $x ]] && kill $x
}

# restart affected services
svr 0 /etc/init.d/apache2 stop
svr 1 /etc/init.d/postfix restart
svr 1 /etc/init.d/apache2 start
exit $rc

# alternatively, do it manually…
print -ru2 "W: reboot this system within the next four weeks!"
exit 0
